FROM tensorflow/tensorflow:2.5.0-gpu

RUN apt-get update \
    && apt-get install -y --no-install-recommends p7zip-full\
    && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY pima-indians-diabetes.csv ./pima-indians-diabetes.csv
COPY main.py ./main.py

CMD [ "python", "-u", "./main.py"]